package com.bank.twitter.api;

import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class ErrorHandlerTest {
    private final ErrorHandler errorHandler = new ErrorHandler();

    @Test
    public void shouldProcessRuntimeException() {
        ResponseEntity<ErrorResponse> response = errorHandler.processRuntimeException(new RuntimeException("Fatal error"));

        assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
        assertThat(response.getBody().getMessage()).isEqualTo("Generic error");
        assertThat(response.getBody().getErrors()).hasSize(1);
        assertThat(response.getBody().getErrors().get(0)).isEqualTo("Fatal error");
    }

    @Test
    public void shouldProcessIllegalArgumentException() {
        ResponseEntity<ErrorResponse> response = errorHandler.procesIllegalArgumentException(new IllegalArgumentException("Wrong param"));

        assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("Validation error");
        assertThat(response.getBody().getErrors()).hasSize(1);
        assertThat(response.getBody().getErrors().get(0)).isEqualTo("Wrong param");
    }

    @Test
    public void shouldProcessArgumentValidationErrors() {
        MethodArgumentNotValidException exception = givenValidationException();

        ResponseEntity<ErrorResponse> response = errorHandler.processArgumentValidationErrors(exception);

        assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("Validation error");
        assertThat(response.getBody().getErrors()).hasSize(2);
        assertThat(response.getBody().getErrors().get(0)).isEqualTo("date: cannot be null");
        assertThat(response.getBody().getErrors().get(1)).isEqualTo("age: should be at least 12");
    }

    private MethodArgumentNotValidException givenValidationException() {
        List<FieldError> validationErrors = Arrays.asList(
                new FieldError("object", "date", "cannot be null"),
                new FieldError("object", "age", "should be at least 12")
        );

        BindingResult bindingResult = mock(BindingResult.class);
        given(bindingResult.getFieldErrors()).willReturn(validationErrors);

        MethodArgumentNotValidException exception = mock(MethodArgumentNotValidException.class);
        given(exception.getBindingResult()).willReturn(bindingResult);
        return exception;
    }
}