package com.bank.twitter.api.message;

import com.bank.twitter.domain.Id;
import com.bank.twitter.domain.message.Message;
import com.bank.twitter.domain.message.MessageService;
import com.bank.twitter.domain.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class MessageControllerTest {
    private static final Id USER_ID = Id.from("123");

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private MessageService messageService;

    @Test
    public void shouldCreateMessage() throws Exception {
        PostMessageRequest givenRequest = new PostMessageRequest("message");

        ResultActions result = mvc.perform(
                post("/users/{userId}/messages", USER_ID)
                        .content(objectMapper.writeValueAsString(givenRequest))
                        .contentType(MediaType.APPLICATION_JSON)
        );

        result.andExpect(status().isNoContent());
        then(messageService).should().createMessage(USER_ID, "message");
    }

    @Test
    public void shouldGetCreatedMessagesForUser() throws Exception {
        given(messageService.getMessages(USER_ID)).willReturn(messages());

        ResultActions result = mvc.perform(get("/users/{userId}/messages", USER_ID));

        result.andExpect(status().isOk())
                .andExpect(content().string(json(MessagesResponse.from(messages()))));
    }

    @Test
    public void shouldGeTimelineForUser() throws Exception {
        given(messageService.getTimeline(USER_ID)).willReturn(messages());

        ResultActions result = mvc.perform(get("/users/{userId}/messages/timeline", USER_ID));

        result.andExpect(status().isOk())
                .andExpect(content().string(json(MessagesResponse.from(messages()))));
    }

    private String json(Object object) throws Exception {
        return objectMapper.writeValueAsString(object);
    }

    private List<Message> messages() {
        return Arrays.asList(
                new Message(new User(USER_ID), "message 1"),
                new Message(new User(USER_ID), "message 2")
        );
    }
}