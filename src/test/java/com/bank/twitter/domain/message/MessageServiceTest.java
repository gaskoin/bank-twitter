package com.bank.twitter.domain.message;

import com.bank.twitter.domain.Id;
import com.bank.twitter.domain.user.UserService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageServiceTest {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    private Id userId = new Id();

    @Test
    public void shouldCreateMessage() {
        messageService.createMessage(userId, "good morning");

        List<Message> messages = messageService.getMessages(userId);

        assertThat(messages).hasSize(1);
        Assertions.assertThat(messages.get(0).getUser().getId()).isEqualTo(userId);
        assertThat(messages.get(0).getText()).isEqualTo("good morning");
    }

    @Test
    public void shouldGetMessagesFromRepositoryInReverseChronologicalOrder() {
        messageService.createMessage(userId, "hello world");
        messageService.createMessage(userId, "nice weather");

        List<Message> messages = messageService.getMessages(userId);

        assertMessages(messages);
    }

    @Test
    public void shouldGetTimelineInReverseChronologicalOrder() {
        Id interestingPerson = new Id();
        userService.follow(userId, interestingPerson);

        messageService.createMessage(interestingPerson, "hello world");
        messageService.createMessage(interestingPerson, "nice weather");

        List<Message> messages = messageService.getTimeline(userId);

        assertMessages(messages);
    }

    private void assertMessages(List<Message> messages) {
        assertThat(messages).hasSize(2);
        assertThat(messages.get(0).getText()).isEqualTo("nice weather");
        assertThat(messages.get(1).getText()).isEqualTo("hello world");
    }
}