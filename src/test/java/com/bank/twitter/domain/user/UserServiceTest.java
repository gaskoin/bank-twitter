package com.bank.twitter.domain.user;

import com.bank.twitter.domain.Id;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {
    private static final Id EXISTING_USER = Id.from("john");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void shouldFindExistingUserInDatabase() {
        User user = userService.find(EXISTING_USER);

        assertThat(user).isNotNull();
    }

    @Test
    public void shouldCreateUserInDatabaseIfNotFound() {
        User user = userService.find(new Id());

        assertThat(user).isNotNull();
    }

    @Test
    public void shouldNotAllowToFollowSelf() {
        Id sameId = new Id();

        Assertions.assertThatThrownBy(() -> userService.follow(sameId, sameId))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("You cannot follow yourself");
    }

    @Test
    public void shouldFollowAnotherUser() {
        Id followerId = new Id();
        Id followingId = new Id();
        userService.follow(followerId, followingId);

        User follower = userService.find(followerId);
        User following = userService.find(followingId);

        assertFollower(follower, following);
        assertFollowing(follower, following);
    }

    private void assertFollower(User follower, User following) {
        assertThat(follower.getFollowers()).isEmpty();
        assertThat(follower.getFollowings()).hasSize(1);
        assertThat(follower.getFollowings()).contains(following);
    }

    private void assertFollowing(User follower, User following) {
        assertThat(following.getFollowers()).hasSize(1);
        assertThat(following.getFollowers()).contains(follower);
        assertThat(following.getFollowingsIds()).isEmpty();
    }
}