package com.bank.twitter.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.status;


@ControllerAdvice
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ErrorHandler {
    private static final String VALIDATION_ERROR = "Validation error";
    private static final String GENERIC_ERROR = "Generic error";

    @ExceptionHandler(RuntimeException.class)
    ResponseEntity<ErrorResponse> processRuntimeException(RuntimeException exception) {
        log.error("Error", exception);
        return status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorResponse.from(GENERIC_ERROR, singletonList(exception.getMessage())));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<ErrorResponse> procesIllegalArgumentException(RuntimeException exception) {
        log.warn(VALIDATION_ERROR, exception);
        return badRequest().body(ErrorResponse.from(VALIDATION_ERROR, singletonList(exception.getMessage())));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ErrorResponse> processArgumentValidationErrors(MethodArgumentNotValidException exception) {
        log.warn(VALIDATION_ERROR, exception);

        final List<String> errors = exception.getBindingResult().getFieldErrors()
                .stream()
                .map(this::formatError)
                .collect(Collectors.toList());

        return badRequest().body(ErrorResponse.from(VALIDATION_ERROR, errors));
    }

    private String formatError(FieldError error) {
        return String.format("%s: %s", error.getField(), error.getDefaultMessage());
    }
}
