package com.bank.twitter.api.user;

import com.bank.twitter.domain.Id;
import com.bank.twitter.domain.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.noContent;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PutMapping("/users/{userId}/followings/{followingId}")
    public ResponseEntity<Void> followUser(@PathVariable Id userId, @PathVariable Id followingId) {
        userService.follow(userId, followingId);
        return noContent().build();
    }
}
