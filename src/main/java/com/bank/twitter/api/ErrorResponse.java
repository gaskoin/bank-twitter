package com.bank.twitter.api;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
@RequiredArgsConstructor(staticName = "from")
@Builder
class ErrorResponse {
    String message;
    List<String> errors;
}
