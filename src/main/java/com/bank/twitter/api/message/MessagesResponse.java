package com.bank.twitter.api.message;

import com.bank.twitter.domain.message.Message;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value
class MessagesResponse {
    List<MessageResponse> messages;

    static MessagesResponse from(List<Message> messages) {
        return new MessagesResponse(
                messages.stream()
                        .map(MessageResponse::from)
                        .collect(Collectors.toList())
        );
    }
}
