package com.bank.twitter.api.message;

import com.bank.twitter.domain.message.Message;
import lombok.Value;

@Value
class MessageResponse {
    String userId;
    String text;

    static MessageResponse from(Message message) {
        return new MessageResponse(message.getUserId(), message.getText());
    }
}
