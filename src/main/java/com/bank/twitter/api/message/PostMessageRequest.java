package com.bank.twitter.api.message;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Value
@AllArgsConstructor
@NoArgsConstructor(force = true)
class PostMessageRequest {
    @Size(min = 1, max = 140)
    @NotBlank
    private String text;
}