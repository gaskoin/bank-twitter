package com.bank.twitter.api.message;

import com.bank.twitter.domain.Id;
import com.bank.twitter.domain.message.Message;
import com.bank.twitter.domain.message.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.noContent;

@RestController
@RequestMapping(path = "/users/{userId}")
@RequiredArgsConstructor
class MessageController {
    private final MessageService messageService;

    @PostMapping("/messages")
    public ResponseEntity<Void> postMessage(@PathVariable Id userId, @Valid @RequestBody PostMessageRequest request) {
        messageService.createMessage(userId, request.getText());
        return noContent().build();
    }

    @GetMapping("/messages")
    public ResponseEntity<MessagesResponse> getMessages(@PathVariable Id userId) {
        List<Message> messages = messageService.getMessages(userId);
        return ResponseEntity.ok(MessagesResponse.from(messages));
    }

    @GetMapping("/messages/timeline")
    public ResponseEntity<MessagesResponse> getTimeline(@PathVariable Id userId) {
        List<Message> messages = messageService.getTimeline(userId);
        return ResponseEntity.ok(MessagesResponse.from(messages));
    }

}
