package com.bank.twitter.domain.user;

import com.bank.twitter.domain.Id;
import org.springframework.data.repository.CrudRepository;

interface UserRepository extends CrudRepository<User, Id> {
}
