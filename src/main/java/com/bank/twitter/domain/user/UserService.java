package com.bank.twitter.domain.user;

import com.bank.twitter.domain.Id;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {
    private final UserRepository userRepository;

    public void follow(Id followerId, Id followingId) {
        if (followerId.equals(followingId)) {
            throw new IllegalArgumentException("You cannot follow yourself");
        }

        User follower = find(followerId);
        User following = find(followingId);

        follower.follow(following);
        userRepository.save(follower);
    }

    public User find(Id userId) {
        return userRepository.findById(userId)
                .orElseGet(() -> userRepository.save(new User(userId)));
    }
}
