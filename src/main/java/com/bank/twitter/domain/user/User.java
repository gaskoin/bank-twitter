package com.bank.twitter.domain.user;

import com.bank.twitter.domain.Id;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Data
@EqualsAndHashCode(exclude = {"followers", "followings"})
@ToString(exclude = {"followers", "followings"})
@Entity
@Table(name = "users")
public class User {
    @EmbeddedId
    private Id id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_followers",
            joinColumns = {@JoinColumn(name = "follower_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<User> followers = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_followers",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "follower_id")})
    private Set<User> followings = new HashSet<>();

    public User() {
        id = new Id();
    }

    public User(Id id) {
        this.id = id;
    }

    void follow(User followingUser) {
        followings.add(followingUser);
    }

    public Set<Id> getFollowingsIds() {
        return followings.stream()
                .map(User::getId)
                .collect(toSet());
    }
}
