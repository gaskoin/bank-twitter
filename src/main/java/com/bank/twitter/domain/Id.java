package com.bank.twitter.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
@EqualsAndHashCode
@AllArgsConstructor(staticName = "from")
public class Id implements Serializable {
    private static final long serialVersionUID = 0L;

    private final String id;

    public Id() {
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        return id;
    }
}
