package com.bank.twitter.domain.message;

import com.bank.twitter.domain.user.User;
import com.bank.twitter.domain.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

@Data
@Entity
@Table(name = "messages")
@NoArgsConstructor
public class Message {
    @EmbeddedId
    private Id id;

    @ManyToOne
    private User user;

    @Size(max = 140)
    private String text;

    @NotNull
    private Instant created = Instant.now();

    public Message(User user, String text) {
        this.id = new Id();
        this.user = user;
        this.text = text;
    }

    public String getUserId() {
        return user.getId().toString();
    }
}