package com.bank.twitter.domain.message;

import com.bank.twitter.domain.user.User;
import com.bank.twitter.domain.Id;
import com.bank.twitter.domain.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final MessageRepository messageRepository;
    private final UserService userService;

    public void createMessage(Id userId, String text) {
        User user = userService.find(userId);
        Message message = new Message(user, text);
        messageRepository.save(message);
    }

    public List<Message> getMessages(Id userId) {
        return messageRepository.findByUser_IdOrderByCreatedDesc(userId);
    }

    public List<Message> getTimeline(Id userId) {
        User user = userService.find(userId);
        Set<Id> userIds = user.getFollowingsIds();
        return messageRepository.findAllByUser_IdOrderByCreatedDesc(userIds);
    }
}
