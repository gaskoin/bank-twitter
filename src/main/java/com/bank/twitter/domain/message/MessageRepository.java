package com.bank.twitter.domain.message;

import com.bank.twitter.domain.Id;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface MessageRepository extends CrudRepository<Message, Id> {
    List<Message> findByUser_IdOrderByCreatedDesc(Id userId);

    List<Message> findAllByUser_IdOrderByCreatedDesc(Iterable<Id> userIds);
}
