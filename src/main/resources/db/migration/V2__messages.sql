CREATE TABLE messages
(
     id      VARCHAR(36)    PRIMARY KEY NOT NULL,
     user_id VARCHAR(36)                NOT NULL,
     text    VARCHAR(140)               NOT NULL,
     created TIMESTAMP                  NOT NULL,
     CONSTRAINT fk_messages_users FOREIGN KEY (user_id) REFERENCES users (id)
);