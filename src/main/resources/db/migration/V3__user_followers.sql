CREATE TABLE user_followers
(
     user_id     VARCHAR(36) NOT NULL,
     follower_id VARCHAR(36) NOT NULL,
     CONSTRAINT fk_following_users FOREIGN KEY (user_id) REFERENCES users (id),
     CONSTRAINT fk_follower_users FOREIGN KEY (follower_id) REFERENCES users (id),
     PRIMARY KEY (user_id, follower_id)
);