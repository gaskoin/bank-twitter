# Twitter

### Things to know:
* [REST Api Description](https://bitbucket.org/gaskoin/bank-twitter/wiki/Home)
* Messages could be tied to `User` entity. This will make this implementation less complex but also less extensible 
* I was considering groovy for tests but requirement clearly stated that I need to use JAVA
* I decided to use spring-data with in memory HSQL database instead of some custom in memory implementation. 
This affects an implementation a bit but is somehow more real-like
* Controllers are tested only for interactions. No e2e testing
* I started with unit testing of `UserService` and `MessageService` but most of code was repository mocking. Finally I decided to test service in integration with database which actually made tests more readable
* In test package there is one migration that adds user for one test 

### Build

In order to run build:
```
mvn clean install
```

### Further Improvements:
* paging the results
* sorting as an API parameter
* better error handling
* logging configuration
* test more worse case scenarios
* consider controller tests as e2e tests
* Document API by swagger
* ...